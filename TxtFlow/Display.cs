﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace PixelDisp
{
    public partial class Display : UserControl
    {

        
        private Pixel[,] displayMatrix;
        
        private char[,] displayMatrixData;
       
        private int[] resolution = new int[2];
        
        private PixelType pixelType;
       
        private Color pixelColor;
       
        private string text;
       
        private Font textFont;

    
        private int columnPointer;
        

        private System.ComponentModel.Container myContainer = new System.ComponentModel.Container();
        public Display()
        {
            initDisplay();
            InitializeComponent();
        }
        
        public Display(IContainer container)
        {
            container.Add(this);
            initDisplay();
            InitializeComponent();
        }

        private void initDisplay()
        {
            
            resolution[0] = 32;
            resolution[1] = 8;
            displayMatrixData = new char[resolution[0], resolution[1]];
            displayMatrix = new Pixel[resolution[0], resolution[1]];
            pixelType = PixelType.Oval;
            pixelColor = Color.Red;
            textFont = new Font(new FontFamily("Calibri"),8,FontStyle.Regular);
            int winheight = Size.Height / resolution[1];
            int winwidth = Size.Width / resolution[0];
            for (int i = 0; i < resolution[0]; i++)
            {
                for (int j = 0; j < resolution[1]; j++)
                {
                    displayMatrix[i,j] = new Pixel(this.myContainer);
                    displayMatrix[i,j].newPixelState = PixelState.Off;
                    displayMatrix[i,j].newPixelType = pixelType;
                    displayMatrix[i,j].newPixelColor = pixelColor;
                    displayMatrix[i,j].SetBounds(ClientRectangle.Left, ClientRectangle.Top, 15, 15);
                    displayMatrix[i, j].Location = new System.Drawing.Point(i * winwidth,  j * winheight);
                    this.Controls.Add(displayMatrix[i, j]);
                }
            }
        }
        
        private void reInitDisplay(int oldResX, int oldResY)
        {
            int winheight = Size.Height / resolution[1];
            int winwidth = Size.Width / resolution[0];
            int pitch = winheight < winwidth ? winheight : winwidth;
            displayMatrixData = new char[resolution[0], resolution[1]];
            for(int i=0;i<oldResX; i++)
            {
                for (int j=0; j<oldResY; j++)
                {
                    this.Controls.Remove(displayMatrix[i, j]);
                    myContainer.Remove(displayMatrix[i, j]);
                    displayMatrix[i, j].Dispose();
                }
            }

            for(int i=0; i<resolution[0]; i++)
            {
                for(int j = 0; j < resolution[1]; j++)
                {
                    displayMatrix[i, j] = new Pixel(this.myContainer);
                    displayMatrix[i, j].newPixelState = PixelState.Off;
                    displayMatrix[i, j].newPixelType = pixelType;
                    displayMatrix[i, j].newPixelColor = pixelColor;
                    displayMatrix[i, j].SetBounds(ClientRectangle.Left, ClientRectangle.Top, pitch - 1, pitch - 1);
                    displayMatrix[i, j].Location= new System.Drawing.Point(i * pitch, j * pitch);
                    this.Controls.Add(displayMatrix[i, j]);
                }
            }
        }
        
        private void reInitDisplay()
        {
            int winheight = Size.Height / resolution[1];
            int winwidth = Size.Width / resolution[0];
            int pitch = winheight < winwidth ? winheight : winwidth;
            for (int i = 0; i < resolution[0]; i++)
            {
                for (int j = 0; j < resolution[1]; j++)
                {
                    displayMatrix[i, j].newPixelState = PixelState.Off;
                    displayMatrix[i, j].newPixelType = pixelType;
                    displayMatrix[i, j].newPixelColor = pixelColor;
                    displayMatrix[i, j].SetBounds(ClientRectangle.Left, ClientRectangle.Top, pitch - 1, pitch - 1);
                    displayMatrix[i, j].Location = new System.Drawing.Point(i * pitch, j * pitch);
                }
            }
            putT(0);
            putE(6);
            putS(11);
            putT(17);

        }
        //something to display
        private void putT(int n)
        {
            displayMatrix[n, 0].newPixelState = PixelState.On;
            displayMatrix[n+1, 0].newPixelState = PixelState.On;
            displayMatrix[n+3, 0].newPixelState = PixelState.On;
            displayMatrix[n+4, 0].newPixelState = PixelState.On;
            for(int i = 0; i < 8; i++)
                displayMatrix[n + 2, i].newPixelState = PixelState.On;

        }

        private void putE(int n)
        {
            for (int i = 0; i < 8; i++)
                displayMatrix[n, i].newPixelState = PixelState.On;

            for (int i = 1; i < 4; i++)
                displayMatrix[n + i, 0].newPixelState = PixelState.On;

            for (int i = 1; i < 4; i++)
                displayMatrix[n + i, 3].newPixelState = PixelState.On;

            for (int i = 1; i < 4; i++)
                displayMatrix[n + i, 7].newPixelState = PixelState.On;

        }

        private void putS(int n)
        {
            displayMatrix[n, 2].newPixelState = PixelState.On;
            displayMatrix[n, 5].newPixelState = PixelState.On;

            displayMatrix[n+1, 1].newPixelState = PixelState.On;
            displayMatrix[n+1, 3].newPixelState = PixelState.On;
            displayMatrix[n+1, 6].newPixelState = PixelState.On;

            displayMatrix[n + 2, 0].newPixelState = PixelState.On;
            displayMatrix[n + 2, 3].newPixelState = PixelState.On;
            displayMatrix[n + 2, 7].newPixelState = PixelState.On;

            displayMatrix[n + 3, 0].newPixelState = PixelState.On;
            displayMatrix[n + 3, 3].newPixelState = PixelState.On;
            displayMatrix[n + 3, 7].newPixelState = PixelState.On;

            displayMatrix[n + 4, 1].newPixelState = PixelState.On;
            displayMatrix[n + 4, 4].newPixelState = PixelState.On;
            displayMatrix[n + 4, 6].newPixelState = PixelState.On;

            displayMatrix[n + 5, 2].newPixelState = PixelState.On;
            displayMatrix[n + 5, 5].newPixelState = PixelState.On;


        }

        

        protected override void OnClientSizeChanged(EventArgs e)
        {
            base.OnClientSizeChanged(e);
            reInitDisplay();
        }
        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            reInitDisplay();
        }
        
        [Category("A_Display"),
            Description ("Defines display pixel types")]
        public PixelType DisplayPixelType
        {
            get
            {
                return pixelType;
            }
            set
            {
                pixelType = value;
                reInitDisplay();
                Invalidate();
            }
        }
        [Category("A_Display"),
         Description("Defines display pixel colour")]
        public Color DisplayPixelColor
        {
            get
            {
                return pixelColor;
            }
            set
            {
                pixelColor = value;
                reInitDisplay();
                Invalidate();
            }
        }
        [Category("A_Display"),
        Description("Defines display pixel colour")]
        public string DisplayText
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
                Invalidate();
            }
        }
        [Category("A_Display"),
        Description("Defines display font")]
        public Font DisplayFont
        {
            get
            {
                return textFont;
            }
            set
            {

                
                textFont = value;
                Invalidate();
            }
        }

        private void Display_Load(object sender, EventArgs e)
        {

        }
    }
}
