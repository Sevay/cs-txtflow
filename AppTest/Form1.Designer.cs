﻿namespace Testowa
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.display1 = new PixelDisp.Display(this.components);
            this.SuspendLayout();
            // 
            // display1
            // 
            this.display1.AutoScroll = true;
            this.display1.DisplayFont = new System.Drawing.Font("Arial Narrow", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.display1.DisplayPixelColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.display1.DisplayPixelType = PixelDisp.PixelType.Square;
            this.display1.DisplayText = "AAAA";
            this.display1.ImeMode = System.Windows.Forms.ImeMode.On;
            this.display1.Location = new System.Drawing.Point(43, 48);
            this.display1.Margin = new System.Windows.Forms.Padding(0);
            this.display1.Name = "display1";
            this.display1.Size = new System.Drawing.Size(652, 313);
            this.display1.TabIndex = 0;
            this.display1.TabStop = false;
            this.display1.Load += new System.EventHandler(this.display1_Load);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 253);
            this.Controls.Add(this.display1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private PixelDisp.Display display1;
    }
}

